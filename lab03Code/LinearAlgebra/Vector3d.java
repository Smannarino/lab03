//settimio mannarino
//1738233
package LinearAlgebra;
import java.lang.Math;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x,double y,double z){
        this.x=x;
        this.y=y;
        this.z=z;
    }
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }
    public double magnitude(){
        //performing the square root of x^2+y^2+z^2
        return Math.sqrt((Math.pow(this.x,2))+(Math.pow(this.y,2))+(Math.pow(this.z,2)));
    }
    public double dotProduct(Vector3d vect){
        return (this.x*vect.getX())+(this.y*vect.getY())+(this.z*vect.getZ());
    }
    public Vector3d add(Vector3d vect){
        double newX=this.x + vect.getX();
        double newY=this.y + vect.getY();
        double newZ=this.z + vect.getZ();
        Vector3d newVector=new Vector3d(newX, newY, newZ);
        return newVector;
    }
}
