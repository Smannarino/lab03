//settimio mannarino
//1738233
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {
     // 
     @Test
     public void test() {
        Vector3d vec=new Vector3d(2.5, 3.6, 7.8);
        assertEquals(2.5,vec.getX());
        assertEquals(3.6,vec.getY());
        assertEquals(7.8,vec.getZ());
     }
 
     @Test
     // 
     public void test2() {
         Vector3d vect=new Vector3d(6.0, 6.0, 3.0);
         assertEquals(9,vect.magnitude());
     }
      // 
    @Test
    public void test3() {
        Vector3d vect1=new Vector3d(1.0, 2.0, 3.0);
        Vector3d vect2=new Vector3d(2.0, 4.0, 6.0);
        assertEquals(28.0,vect1.dotProduct(vect2));
    }

    @Test
    // 
    public void test4() {
        Vector3d vect1=new Vector3d(1.5, 6.0, 3.4);
        Vector3d vect2=new Vector3d(3.0, 4.6, 5.3);
        Vector3d addedVect=vect1.add(vect2);
        Vector3d hardCodeResultVect=new Vector3d(4.5, 10.6, 8.7);
        assertEquals(hardCodeResultVect.getX(),addedVect.getX());
        assertEquals(hardCodeResultVect.getY(),addedVect.getY());
        assertEquals(hardCodeResultVect.getZ(),addedVect.getZ());
    }
}
